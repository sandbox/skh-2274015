<?php
/**
 * @file
 * bootstrap_style_carousel.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bootstrap_style_carousel_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_description|field_collection_item|field_slide|default';
  $field_group->group_name = 'group_description';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_slide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Description',
    'weight' => '1',
    'children' => array(
      0 => 'field_title',
      1 => 'field_link',
      2 => 'field_description',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Description',
      'instance_settings' => array(
        'classes' => '',
        'wrapper' => 'footer',
      ),
      'formatter' => '',
    ),
  );
  $export['group_description|field_collection_item|field_slide|default'] = $field_group;

  return $export;
}
