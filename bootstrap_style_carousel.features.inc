<?php
/**
 * @file
 * bootstrap_style_carousel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bootstrap_style_carousel_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
