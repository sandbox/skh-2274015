<?php

/**
 * Implementation of theme_field__FIELD_NAME().
 *
 * A slide field collection and it's children fields.
 */
function ct_field__field_slide(&$variables) {

  // Only alter the top-level slide field collection.
  if ($variables['element']['#field_type'] == 'field_collection') {

    // The renderable content.
    $content = array(

      // Wrap for bootstrap.
      '#prefix' => '<section class="carousel slide" data-ride="carousel">',
      '#suffix' => '</section>',

      // The indicators.
      'indicators' => array(
        '#theme' => 'item_list',
        '#type' => 'ol',
        '#attributes' => array(
          'class' => array('carousel-indicators'),
        ),
        '#items' => array(),
      ),

      // The slides.
      'slides' => array(
        '#prefix' => '<div class="carousel-inner clearfix">',
        '#suffix' => '</div>',
        $variables['items'],
      ),
    );

    // Add an indicator for each slide.
    for ($i = 0; $i < count($variables['items']); $i++) {

      // Create the list item.
      $indicator = array(
        'data' => '',
        'data-target' => '.carousel',
        'data-slide-to' => $i
      );

      // Add active class if necessary.
      if ($i == 0) {
        $indicator['class'] = array('active');
      }

      // Add the list item to the indicators.
      $content['indicators']['#items'][] = $indicator;
    }

    // Remove indicators if there's not multiple slides.
    if (count($content['indicators']['#items']) < 2) {
      unset($content['indicators']);
    }

    // Return the rendered content.
    return render($content);
  }
}

/**
 * Implementation of theme_field_collection_item__FIELD_NAME().
 *
 * A slide item.
 */
function ct_field_collection_item__field_slide(&$variables) {
  static $first_slide;

  $first = !isset($first_slide)? ' active' : '';
  $first_slide = FALSE;

  // The slide item.
  $content = array(
    '#prefix' => '<article class="item' . $first . '">',
    '#suffix' => '</article>',
    $variables['content'],
  );

  // Return the rendered content.
  return render($content);
}

/**
 * Implementation of theme_field__FIELD_NAME().
 *
 * The title on slides.
 */
function ct_field__field_image__field_slide(&$variables) {
  return render($variables['items'][0]);
}

/**
 * Implementation of theme_field__FIELD_NAME().
 *
 * The title on slides.
 */
function ct_field__field_title__field_slide(&$variables) {
  return '<h3>' . $variables['items'][0]['#markup'] . '</h3>';
}

/**
 * Implementation of theme_field__FIELD_NAME().
 *
 * The description on slides.
 */
function ct_field__field_description__field_slide(&$variables) {
  return '<div class="description">' . $variables['items'][0]['#markup'] . '</div>';
}

/**
 * Implementation of theme_field__FIELD_NAME().
 *
 * The link on slides.
 */
function ct_field__field_link__field_slide(&$variables) {
  return '<div class="link">' . render($variables['items'][0]) . '</div>';
}
